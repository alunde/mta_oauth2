#!/bin/bash

#Take a look and see if you can bring the final blow.  Inspired by
# 
# 
#https://help.sap.com/viewer/4505d0bdaf4948449b7f7379d24d0f0d/2.0.01/en-US/af0c9701740c4fa4b495ff5bf96e186a.html
# 
#and
# 
#https://github.com/cloudfoundry/uaa/blob/master/docs/UAA-APIs.rst#non-browser-requests-code-get-oauth-authorize
# 
# 
#https://jwt.io
#

clear

proxy='-x localhost:8888 '
# proxy='-x mitm.sfphcp.com:8888 '
# proxy=""

#resource='https://hxe2.sfphcp.com:51022/auth.xsodata/'
#resource2=$resource'temp/'
#client='sb-na-6e788220-216b-4e9b-a78a-6595323a2924%21u1'
#host=hxe2.sfphcp.com
#port=':30032'
#user=XSA_DEV
#pass=Welcome9

resource='https://test-lmi.dnn.galaxy.ai/oshot/oauth/get'
resource2=$resource
client='sb-na-9f11557c-8aa7-4f20-86be-265b552c62ea'
host='uaa-server.dnn.galaxy.ai'
port=''
user=XSA_ADMIN
pass=W3lc0m31



echo ""
echo ""
echo ""

rm -f cookies.txt
touch cookies.txt

echo "Cookies Cleared."

echo ""
echo ""

echo "First Request Resource:"

echo ""

echo 'curl '$proxy'-k -L -H "Accept:application/json" "'$resource'" --cookie cookies.txt --cookie-jar cookies.txt'

echo ""
echo ""

#This block seemed to be needed if at first a resource was not requested, kicking off a login flow.
#
#echo "First"
#
#echo ""
#
#echo 'curl '$proxy'-k -L -H "Accept:application/json" "https://'$host''$port'/uaa-security/oauth/authorize?response_type=code&client_id='$client'&redirect_uri=https%3A%2F%2F'$host'%3A51022%2Flogin%2Fcallback" --cookie cookies.txt --cookie-jar cookies.txt'
#
#echo ""
#echo ""

echo "Then Request Token Passing in Csrf: Cut&Paste with X-Uaa-Csrf from prior response:"

echo ""

echo 'curl '$proxy'-k -L -H "Accept:application/json" https://'$host''$port'/uaa-security/login.do -H "Referer: https://'$host''$port'/uaa-security/login" -H "Accept: application/json" -H "Content-Type: application/x-www-form-urlencoded" -d "username='$user'&password='$pass'&X-Uaa-Csrf='
echo ""
echo '" --cookie cookies.txt --cookie-jar cookies.txt'

echo ""
echo ""

echo "Finally make new requests:"

echo ""

echo 'curl '$proxy'-k -L -H "Accept:application/json" '$resource' --cookie cookies.txt --cookie-jar cookies.txt'

echo ""

echo 'curl '$proxy'-k -L -H "Accept:application/json" '$resource2' --cookie cookies.txt --cookie-jar cookies.txt'

echo ""
echo ""
